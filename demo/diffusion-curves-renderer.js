import {createBezierBuilder} from "./libDump.adaptive-bezier-curve.js";

/**
 *
 * @param curves: [curve,...]
 *   curve: [startPoint, startGrip, startColorLeft, startColorRight, endColorLeft, endColorRight, endGrip, endPoint]
 *     point: [x,y]
 *     grip: [x,y]
 *     color: [R,G,B,A,P*,F*] ( *advColor mode only, P:diffusionPower,F:Fuzziness or blur size )
 * @param options
 *   context: canvas webgl context, default: auto-create one and return a b64image
 *   advColor: P,F,PF,false default: false
 *   subRatio: curves subdivision ratio (how many segments for each curve, strange segmentation from adaptive-bezier-curve module) default: 1
 *   pass: pass number, default: 8
 *   width: default: given by context or default created context (browser dependant, usually 300px)
 *   height: default: given by context or default created context (browser dependant, usually 150px)
 */
export function frender(curves, options) {
	return (new Renderer(options)).render(curves);
}

class VertexFormat {
	constructor(gl) {
		this.gl = gl;
		this.vao = null;
		this.attributes = [];
		this.size = 0;
	}

	addAttribute(index, name, params = {}) {
		const attribute = {
			index, name,
			dim: 4,
			type: this.gl.FLOAT,
			normalize: false,
			offset: this.size,
			...params
		};
		this.attributes.push(attribute)
		this.size += 4 * attribute.dim;
	}

	setup() {
		if(!this.vao) {
			this.vao = this.gl.createVertexArray();
			this.gl.bindVertexArray(this.vao);
			for(let attr of this.attributes) {
				this.gl.vertexAttribPointer(
					attr.index, //id attribut
					attr.dim, // dimension des vecteurs
					attr.type, // type de donnée
					attr.normalize, // entier à ramener entre 0 et 1 ?
					this.size, // décalage entre 2 attribut (nbr de bit entre les sommets)
					attr.offset); // décalage inital
				this.gl.enableVertexAttribArray(attr.index);
			}
		}
		else {
			this.gl.bindVertexArray(this.vao);
		}
	}
}

export default class Renderer {
	constructor(options) {
		initOptions(this, options);
		this.webglConfig = {};
		initWebGL(this.gl, this.webglConfig);
		this.bezier = createBezierBuilder();
	}

	render(curves) {
		console.log(curves);
		const vertices = arrays2buffer(curves.vertices);
		const indices = new Uint16Array(curves.indices);
		const gl = this.gl;
		const vbo = this.webglConfig.buffers.vertex;
		const ibo = this.webglConfig.buffers.index;

		const nbLayers = this.webglConfig.pingPongTexture.nbLayers;
		const conf = this.webglConfig;

		gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
		conf.voronoiVertexFormat.setup();

		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);

		gl.enable(gl.DEPTH_TEST);
		gl.depthFunc(gl.LEQUAL);

		gl.bindFramebuffer(gl.FRAMEBUFFER, conf.frameBuffer.voronoi);
		gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D,
			conf.antialiasingTexture, 0);
		for(let i of range(conf.pingPongTexture.nbLayers)) {
			gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT1 + i, gl.TEXTURE_2D,
				this.webglConfig.pingPongTexture.active(i), 0);
		}
		gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D,
			this.webglConfig.depthTexture, 0);

		gl.drawBuffers(range(gl.COLOR_ATTACHMENT0, gl.COLOR_ATTACHMENT0 + conf.pingPongTexture.nbLayers + 1));

		gl.viewport(0, 0, this.width, this.height);

		// Définir la couleur d'effacement comme étant le noir, complètement opaque
		gl.clearColor(0.0, 0.0, 0.0, 1.0); // opaque
		// Effacer le tampon de couleur avec la couleur d'effacement spécifiée
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		// Indiquer à WebGL d'utiliser notre programme pour dessiner
		gl.useProgram(this.webglConfig.shaderProgram.voronoi);
		// Définir les uniformes du shader
		const viewMatrix = [
			2 / this.width, 0, 0, 0,
			0, -2 / this.height, 0, 0,
			0, 0, 2 / this.diagonal, 0,
			-1, 1, -1, 1
		];
		gl.uniformMatrix4fv(
			gl.getUniformLocation(this.webglConfig.shaderProgram.voronoi, "viewMatrix"),
			false,
			viewMatrix);
		gl.drawElements(gl.TRIANGLE_STRIP,
			curves.indices.length,
			gl.UNSIGNED_SHORT,
			0 // depuis le début du buffer
		);


		gl.bindBuffer(gl.ARRAY_BUFFER, this.webglConfig.buffers.fullScreen);
		conf.fullscreenVertexFormat.setup();

		gl.bindFramebuffer(gl.FRAMEBUFFER, this.webglConfig.frameBuffer.pingPong);
		for (let pass = 0; pass < this.nbPass; pass++) {
			for(let i of range(conf.pingPongTexture.nbLayers)) {
				gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0 + i, gl.TEXTURE_2D,
					this.webglConfig.pingPongTexture.other(i), 0);
			}
			gl.drawBuffers(range(gl.COLOR_ATTACHMENT0, gl.COLOR_ATTACHMENT0 + conf.pingPongTexture.nbLayers));
			gl.viewport(0, 0, this.width, this.height);

			gl.useProgram(this.webglConfig.shaderProgram.pingPong);

			gl.activeTexture(gl.TEXTURE0);
			gl.bindTexture(gl.TEXTURE_2D, this.webglConfig.depthTexture);
			for(let i of range(conf.pingPongTexture.nbLayers)) {
				gl.activeTexture(gl.TEXTURE1 + i);
				gl.bindTexture(gl.TEXTURE_2D, this.webglConfig.pingPongTexture.active(i));
			}

			gl.uniform1i(gl.getUniformLocation(this.webglConfig.shaderProgram.pingPong, "color"), 1);
			gl.uniform1i(gl.getUniformLocation(this.webglConfig.shaderProgram.pingPong, "params"), 2);
			gl.uniform1i(gl.getUniformLocation(this.webglConfig.shaderProgram.pingPong, "lighting"), 3);
			gl.uniform1i(gl.getUniformLocation(this.webglConfig.shaderProgram.pingPong, "depth"), 0);
			gl.uniform1f(gl.getUniformLocation(this.webglConfig.shaderProgram.pingPong, "radius"), (this.nbPass - pass) / this.nbPass);
			gl.uniform1f(gl.getUniformLocation(this.webglConfig.shaderProgram.pingPong, "pixelSize"), 1 / this.diagonal);
			gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
			this.webglConfig.pingPongTexture.flip();
		}

		gl.bindFramebuffer(gl.FRAMEBUFFER, null);
		gl.viewport(0, 0, this.width, this.height);
		gl.useProgram(this.webglConfig.shaderProgram.final);
		for (let i of range(nbLayers)) {
			gl.activeTexture(gl.TEXTURE0 + i);
			gl.bindTexture(gl.TEXTURE_2D, this.webglConfig.pingPongTexture.active(i));
		}
		gl.activeTexture(gl.TEXTURE0 + nbLayers);
		gl.bindTexture(gl.TEXTURE_2D, this.webglConfig.antialiasingTexture);
		gl.uniform1i(gl.getUniformLocation(this.webglConfig.shaderProgram.final, "color"), 0);
		gl.uniform1i(gl.getUniformLocation(this.webglConfig.shaderProgram.final, "params"), 1);
		gl.uniform1i(gl.getUniformLocation(this.webglConfig.shaderProgram.final, "lighting"), 2);
		gl.uniform1i(gl.getUniformLocation(this.webglConfig.shaderProgram.final, "antialiasing"), 3);
		gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
	}
}

function initOptions(self, options) {
	self.bezierScale = options.subRatio || 1;
	self.advColor = options.advColor || false;
	self.nbPass = (typeof options.pass !== "undefined") ? options.pass : 8;
	self.gl = getContext(options.context);
	if (options.width) self.gl.canvas.width = options.width;
	if (options.height) self.gl.canvas.height = options.height;
	self.width = self.gl.canvas.width;
	self.height = self.gl.canvas.height;
	self.diagonal = Math.sqrt(self.width * self.width + self.height * self.height);
}

function initWebGL(gl, conf) {
	gl.getExtension('EXT_color_buffer_float');
	conf.ATTRIB_POSITION = 0;
	conf.ATTRIB_COLOR = 1;
	conf.ATTRIB_PARAMS = 2;
	conf.ATTRIB_LIGHTING = 3;
	conf.ATTRIB_ANTIALIASING = 4;
	conf.ATTRIB_TEX_COORD = 5;

	conf.voronoiVertexFormat = new VertexFormat(gl);
	conf.voronoiVertexFormat.addAttribute(conf.ATTRIB_POSITION, "vx_position");
	conf.voronoiVertexFormat.addAttribute(conf.ATTRIB_COLOR, "vx_color");
	conf.voronoiVertexFormat.addAttribute(conf.ATTRIB_PARAMS, "vx_params");
	conf.voronoiVertexFormat.addAttribute(conf.ATTRIB_LIGHTING, "vx_lighting");
	conf.voronoiVertexFormat.addAttribute(conf.ATTRIB_ANTIALIASING, "vx_antialiasing");

	conf.fullscreenVertexFormat = new VertexFormat(gl);
	conf.fullscreenVertexFormat.addAttribute(conf.ATTRIB_POSITION, "vx_position", { dim: 2 });
	conf.fullscreenVertexFormat.addAttribute(conf.ATTRIB_TEX_COORD, "vx_texCoord", { dim: 2 });

	const sp = conf.shaderProgram = {};
	sp.voronoi = voronoiShaderProgram(gl, conf.voronoiVertexFormat);
	sp.pingPong = pingPongShaderProgram(gl, conf.fullscreenVertexFormat);
	sp.final = finalShaderProgram(gl, conf.fullscreenVertexFormat);

	const b = conf.buffers = {};
	b.vertex = gl.createBuffer(); // vertex buffer object // vbo
	b.index = gl.createBuffer(); // index buffer object // ibo
	b.fullScreen = fullScreenBuffer(gl);
	conf.pingPongTexture = new PingPongTexture(gl, 3);
	conf.depthTexture = initDepthTexture(gl);
	conf.antialiasingTexture = createTexture2D(gl, gl.RGBA16F, gl.canvas.width, gl.canvas.height, gl.LINEAR);
	conf.frameBuffer = {};
	conf.frameBuffer.voronoi = gl.createFramebuffer();
	conf.frameBuffer.pingPong = gl.createFramebuffer();
}

function voronoiShaderProgram(gl, conf) {
	const vsSource = `#version 300 es

		uniform mat4 viewMatrix;

		in vec4 vx_position;
		in vec4 vx_color;
		in vec4 vx_params;
		in vec4 vx_lighting;
		in vec4 vx_antialiasing;

		out highp vec4 position;
		out highp vec4 color;
		out highp vec4 params;
		out highp vec4 lighting;
		out highp vec4 antialiasing;

		const mat4 rgbToXyz = mat4(
			0.49, 0.17697, 0, 0,
			0.31, 0.8124, 0.01, 0,
			0.2, 0.01063, 0.99, 0,
			0, 0, 0, 1.0);
		vec4 linearToXyz(vec4 linear) {
			return rgbToXyz * linear / 0.17697;
		}

		const float delta = 6.0 / 29.0;
		const float delta2 = delta * delta;
		const float delta3 = delta2 * delta;
		float lab(float x) {
			return (x > delta3)? pow(x, 1.0 / 3.0): x / (3.0 * delta2) + 4.0 / 29.0;
		}

		vec4 xyzToLab(vec4 xyz) {
			return vec4(1.16 * lab(xyz.y) - .16,
			            5.0 * (lab(xyz.x / .95047) - lab(xyz.y)),
									2.0 * (lab(xyz.y) - lab(xyz.z / 1.08883)),
								  xyz.w);
		}

		vec4 srgbToLab(vec4 srgb) {
			return xyzToLab(linearToXyz(pow(srgb, vec4(vec3(2.2), 1.0))));
		}

		void main (){
			position = vx_position;
			color = srgbToLab(vx_color) * vec4(vec3(vx_params.x), 1);
			params = vx_params;
			lighting = vx_lighting;
			antialiasing = vx_antialiasing;
			gl_Position = viewMatrix * vx_position;
		}
	`;
	const fsSource = `#version 300 es
		in highp vec4 position;
		in highp vec4 color;
		in highp vec4 params;
		in highp vec4 lighting;
		in highp vec4 antialiasing;

		layout(location = 1) out highp vec4 out_color;
		layout(location = 2) out highp vec4 out_params;
		layout(location = 3) out highp vec4 out_lighting;
		layout(location = 0) out highp vec4 out_antialiasing;

		void main (){
			out_color = color;
			out_params = params;
			out_lighting = lighting;
			out_antialiasing = antialiasing;
		}
	`;
	return initShaderProgram(gl, vsSource, fsSource, conf);
}

function pingPongShaderProgram(gl, conf) {
	const pingPongVS = `#version 300 es

		in vec4 vx_position;
		in vec2 vx_texCoord;

		out highp vec2 texCoord;

		void main (){
			texCoord = vx_texCoord;
			gl_Position = vx_position;
		}
		`;
	const pingPongFS = `#version 300 es

		uniform sampler2D color;
		uniform sampler2D params;
		uniform sampler2D lighting;
		uniform sampler2D depth;
		uniform highp float radius;
		uniform highp float pixelSize;

		in highp vec2 texCoord;

		layout(location = 0) out highp vec4 out_color;
		layout(location = 1) out highp vec4 out_params;
		layout(location = 2) out highp vec4 out_lighting;

		highp vec4 blur(in sampler2D layer, vec2 coord, float r) {
			return (
					texture(layer, coord+vec2(r,0.0))
				+ texture(layer, coord-vec2(r,0.0))
				+ texture(layer, coord+vec2(0.0,r))
				+ texture(layer, coord-vec2(0.0,r))
			) / 4.0;
		}

		void main (){
			highp float d = texture(depth, texCoord).r - pixelSize; // Distance à la courbe la plus proche, avec une marge
			highp float r = max(0.0, radius * d * sqrt(2.0));
			//highp float r = radius * texture2D(depth, texCoord).r * sqrt(2.0);
			out_color = blur(color, texCoord, r);
			out_params = blur(params, texCoord, r);
			out_lighting = blur(lighting, texCoord, r);
		}
	`;
	return initShaderProgram(gl, pingPongVS, pingPongFS, conf);
}

function finalShaderProgram(gl, conf) {
	const finalVS = `#version 300 es

		in vec4 vx_position;
		in vec2 vx_texCoord;

		out highp vec2 texCoord;

		void main (){
			texCoord = vx_texCoord;
			gl_Position = vx_position;
		}
	`;
	const finalFS = `#version 300 es

		uniform sampler2D color;
		uniform sampler2D params;
		uniform sampler2D lighting;
		uniform sampler2D antialiasing;

		in highp vec2 texCoord;

		out highp vec4 out_color;

		const highp float delta = 6.0 / 29.0;
		const highp float delta2 = delta * delta;
		highp float lab(highp float x) {
			return (x > delta)? pow(x, 3.0): 3.0 * delta2 * (x - 4.0 / 29.0);
		}

		highp vec4 labToXyz(vec4 color) {
			return vec4(0.95047 * lab((color.s + 0.16) / 1.16 + color.t / 5.0),
			            lab((color.s + .16) / 1.16),
									1.08883 * lab((color.s + 0.16) / 1.16 - color.p / 2.0),
								  color.a);
		}

		const highp mat4 xyzToRgb = mat4(
			0.41847, -0.091169, 0.0009209, 0,
			-0.15866, 0.25243, -0.0025498, 0,
			-0.082835, 0.015708, 0.1786, 0,
			0, 0, 0, 1.0);
		highp vec4 xyzToLinear(vec4 xyz) {
			return xyzToRgb * xyz;
		}

		highp vec4 labToSrgb(vec4 lab) {
			return pow(xyzToLinear(labToXyz(lab)), vec4(vec3(1.0 / 2.2), 1.0));
		}

		highp vec3 lambert(in vec3 n, in vec3 l, in vec3 color) {
			return max(0.0, dot(n, l)) * color;
		}

		highp vec3 blinnPhong(in vec3 n, in vec3 l, in vec3 v, in vec3 color, in float roughness) {
			highp vec3 h = normalize(v + l);
			highp float cosAlpha = max(0.0, dot(h, n));
			return color * pow(cosAlpha, 1.0 / roughness);
		}

		void main (){
			highp vec3 v = vec3(0, 0, 1);
			highp vec3 l = normalize(vec3(1, 1, 1));
			highp vec3 lightColor = 1.0 * vec3(1, 1, 1);
			highp vec3 ambiantColor = vec3(0.0);

			highp vec4 p = texture(params, texCoord);
			highp vec4 lp = texture(lighting, texCoord);

			highp vec3 n = vec3(lp.xy, 0);
			n.z = sqrt(1.0 - n.x * n.x - n.y * n.y);
			highp float shininess = lp.z;
			highp float roughness = lp.w;

			highp vec4 albedo = pow(labToSrgb(texture(color, texCoord)) / vec4(vec3(p.x), 1), vec4(vec3(2.2), 1));
			highp vec3 diffLight = ambiantColor + lambert(n, l, lightColor)
				+ lambert(n, normalize(vec3(-1, 0, .2)), 0.4 * vec3(1, 0.5, 0))
				+ lambert(n, normalize(vec3(1, -8, 1)), 0.4 * vec3(0.25, 0.5, 1));

			highp vec3 specLight = blinnPhong(n, l, v, lightColor * shininess, roughness);

			out_color = pow(albedo, vec4(vec3(1.0 / 2.2), 1)) * vec4(diffLight + specLight, 1);
			//out_color = vec4(texture(params, texCoord).rgb, 1); // Show params layer
			//out_color = vec4(texture(lighting, texCoord).rgb, 1); // Show lighting layer
			//out_color = vec4(n / 2.0 + vec3(.5), 1); // Show normals
			//out_color = vec4(diffLight, 1); // Show lighting
			//out_color = vec4(texture(antialiasing, texCoord).xy / 2.0 + vec2(.5), 0, 1); // Show antialiasing layer
		}
	`;
	return initShaderProgram(gl, finalVS, finalFS, conf);
}

function fullScreenBuffer(gl) {
	const fullScreenBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, fullScreenBuffer);
	const fullScreenQuad = [
		-1, -1, 0, 0, // coord X, Y, U, V (U et V -> coord texture)
		-1, 1, 0, 1,
		1, -1, 1, 0,
		1, 1, 1, 1
	];
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(fullScreenQuad), gl.STATIC_DRAW);
	return fullScreenBuffer;
}

function createTexture2D(gl, format, width, height, filter) {
	const tex = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, tex);
	gl.texStorage2D(gl.TEXTURE_2D, 1, format, width, height);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, filter);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, filter);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
	return tex;
}

class PingPongTexture {
	constructor(gl, nbLayers) {
		const textures = [];
		for (let i of range(nbLayers * 2)) {
			const tex = createTexture2D(gl, gl.RGBA16F, gl.canvas.width, gl.canvas.height, gl.LINEAR);
			textures.push(tex);
		}
		this.nbLayers = nbLayers;
		this.offset = 0;
		this.textures = textures;
	}

	active(i = 0) {
		return this.textures[(i + this.offset) % (this.nbLayers * 2)];
	}

	other(i = 0) {
		return this.textures[(i + this.nbLayers + this.offset) % (this.nbLayers * 2)];
	}

	flip() {
		this.offset = (this.offset + this.nbLayers) % (this.nbLayers * 2);
		return this.active();
	}

	next() {
		return this.flip();
	}
}

function initDepthTexture(gl) {
	const depthTexture = createTexture2D(gl, gl.DEPTH_COMPONENT24, gl.canvas.width, gl.canvas.height, gl.NEAREST);
	return depthTexture;
}

function getContext(optionalContext) {
	if (optionalContext instanceof WebGL2RenderingContext) return optionalContext;
	return document.createElement("canvas").getContext("webgl2");
}

function initShaderProgram(gl, vsSource, fsSource, vertexFormat) { // Initialiser un programme shader, de façon à ce que WebGL sache comment dessiner nos données
	const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource);
	const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource);

	// Créer le programme shader
	const shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	for(let attr of vertexFormat.attributes)
		gl.bindAttribLocation(shaderProgram, attr.index, attr.name);
	gl.linkProgram(shaderProgram);

	// Si la création du programme shader a échoué, alerte
	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		console.error("Impossible d'initialiser le programme shader : " + gl.getProgramInfoLog(shaderProgram));
		return null;
	}
	return shaderProgram;
}

function loadShader(gl, type, source) { // Crée un shader du type fourni, charge le source et le compile.
	const shader = gl.createShader(type);
	gl.shaderSource(shader, source);
	gl.compileShader(shader);
	// Vérifier s'il a ét compilé avec succès
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		console.error('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
		gl.deleteShader(shader);
		return null;
	}
	return shader;
}

function arrays2buffer(array) {
	return new Float32Array([].concat(...array));
}

function normalTo(vec) {
	return new Vec2D.ArrayVector(-vec.getY(), vec.getX());
}

function* range(beginOrEnd, end, step = 1) {
	let begin = beginOrEnd;
	if (end === undefined) {
		end = beginOrEnd;
		begin = 0;
	}
	for (let i = begin; i < end; i += step)
		yield i;
}
