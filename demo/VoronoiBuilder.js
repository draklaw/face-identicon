
function normalTo(vec) {
	return new Vec2D.ArrayVector(-vec.getY(), vec.getX());
}

function verticize(point, deepness, color, dir) {
	if (color.length < 2) color[1] = color[0];
	if (color.length < 3) color[2] = color[1];
	if (color.length < 4) color[3] = 1; //alpha
	if (!color[4]) color[4] = 1; //intensity
	const vertice = [];
	vertice.push(...point.toArray(), deepness, 1,
		color[0], color[1], color[2], color[3],
		color[4], 0, 0, 1, // Weight, blur, normal.xy
		dir.x, dir.y, 1, 1);
	return vertice;
}

function colorMerge(c1, c2, ratio) {
	const c = [];
	const maxDim = Math.max(c1.length, c2.length);
	for (let i = 0; i < maxDim; i++) {
		c[i] =
			(1 - ratio) * (typeof c1[i] !== "undefined" ? c1[i] : 1)
			+
			ratio * (typeof c2[i] !== "undefined" ? c2[i] : 1)
		;
	}
	return c;
}

export default class VoronoiBuilder {
	constructor(radius, tipSplitCount = 4) {
		this.LEFT = 0;
		this.RIGHT = 1;

		this.radius = radius;
		this.tipSplitCount = tipSplitCount;

		this.vertices = [];
		this.indices = [];

		this.nullVector = Vec2D.ArrayVector(0, 0);
	}

	addCurve(points, closed = false) {
		if(points.length < 2)
			return;

		//if(closed) {
			// TODO
		//}
		//else {
			let startVertices = null;
			let currentVertices = null;

			const i1 = points.length - 1;
			const i2 = points.length - 2;

			// Left side
			let p0 = points[0];
			let p1 = points[1];
			if(points.length > 2) {
				for(let i = 2; i < points.length; ++i) {
					const p2 = points[i];

					const [b, e] = this.addCorner(p0, p1, p2, this.LEFT, currentVertices);
					if(!startVertices)
						startVertices = b;
					currentVertices = e;

					p0 = p1;
					p1 = p2;
				}
			}

			// End tip
			currentVertices = this.addTip(p0, p1, this.LEFT, currentVertices);

			// Right side in reverse
			const tmp = p0;
			p0 = p1;
			p1 = tmp;
			for(let j = 1; j < points.length - 1; ++j) {
				const i = points.length - 1 - j;
				const p2 = points[i];

				const [, e] = this.addCorner(p0, p1, p2, this.LEFT, currentVertices);
				currentVertices = e;

				p0 = p1;
				p1 = p2;
		}

			// Begin tip
			currentVertices = this.addTip(p0, p1, this.RIGHT, currentVertices);

			// Last bit
			this.addQuad(currentVertices, startVertices);
		//}
	}

	addQuad(begin, end) {
		this.indices.push(end[0]);
		this.indices.push(end[1]);
	}

	addCorner(p0, p1, p2, side, currentVertices) {
		const v0 = p1.p.clone().subtract(p0.p).normalise();
		const v1 = p2.p.clone().subtract(p1.p).normalise();
		const n0 = normalTo(v0).mulS(this.radius);
		const n1 = normalTo(v1).mulS(this.radius);

		const color = (side == this.LEFT)? p1.leftColor: p1.rightColor;

		const vx0 = this.vertices.length;
		const fanBegin = vx0 + 1;
		const fanEnd   = fanBegin + 1;
		const begin = [ vx0, fanBegin ];
		const end   = [ vx0, fanEnd ];

		this.vertices.push(...verticize(p1.p, 0, color, this.nullVector));
		this.vertices.push(...verticize(p1.p.clone().add(n0), this.radius, color, n0.clone().mulS(-1)));
		// TODO: intermediate vertices if required.
		this.vertices.push(...verticize(p1.p.clone().add(n1), this.radius, color, n0.clone().mulS(-1)));

		if(currentVertices) {
			this.indices.push(vx0, fanBegin, fanEnd, vx0, fanEnd);
		}

		return [ begin, end ];
	}

	addTip(p0, p1, side, currentVertices) {
		const v0 = p1.p.clone().subtract(p0).normalise();
		const n0 = normalTo(v0);

		const color0 = (side == this.LEFT)? p1.leftColor: p1.rightColor;
		const color1 = (side == this.LEFT)? p1.rightColor: p1.leftColor;

		const vx0 = this.vertices.length;
		const fanBegin = vx0 + 1;

		this.vertices.push(...verticize(p1.p, 0, color0, this.nullVector));
		this.vertices.push(...verticize(p1.p.clone().add(n0.clone().mulS(this.radius)), this.radius, color0, n0.clone().mulS(-this.radius)));

		this.indices.push(vx0, fanBegin);

		let fanEnd = fanBegin;
		for(let i = 0; i < this.tipSplitCount; ++i) {
			const t = (i + 1) / this.tipSplitCount;
			const alpha = t * Math.PI;

			const d = n0.clone().mulS(Math.cos(alpha)).add(v0.clone().mulS(Math.sin(alpha)));
			const color = colorMerge(color0, color1, t);

			this.vertices.push(...verticize(p1.p.clone().add(d.clone().mulS(this.radius)), this.radius, color, d.clone().mulS(-this.radius)));

			fanEnd += 1;
			this.indices.push(fanEnd, vx0, fanEnd);
		}

		return [ vx0, fanEnd ];
	}
}
